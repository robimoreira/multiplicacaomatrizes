package main

import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"	
	"log"
)

func main() {

	ArquivoMatriz01 := os.Args[1] 
	ArquivoMatriz02 := os.Args[2]
	ArquivoMatriz03 := os.Args[3]

	matriz01, ordem1 := lerMatriz(ArquivoMatriz01)
	matriz02, ordem2 := lerMatriz(ArquivoMatriz02)
	matriz03 := multiplicacaoMatriz(matriz01, matriz02, ordem1)
	salvarMatriz(matriz03, ordem2, ArquivoMatriz03)
}

func lerMatriz(matriz string) ([]float64, int){
	var linhasMatriz, linhas []string
	var matrizConvertida []float64

	textoMatriz, err := os.Open(matriz)
	if err != nil {
		fmt.Printf(" %v \n", err)
	}

	txtMatriz := bufio.NewScanner(textoMatriz)
	for txtMatriz.Scan() {
		linhasMatriz = append(linhasMatriz, txtMatriz.Text())
	}
	textoMatriz.Close()

	ordem, erro := strconv.Atoi(linhasMatriz[0])
	if(erro != nil){
		fmt.Printf("erro: %v \n", erro)
	}

	for i := 1; i <= ordem; i++{
		aux := linhasMatriz[i]
		linha := strings.Replace(aux, "/n", "", -1)
		linhaNova := strings.Split(linha, ":")
		linhas = append(linhas, linhaNova...)
	}	 
	for _, elem := range linhas{

		i, err := strconv.ParseFloat(elem, 64)
	    if err == nil {
	       matrizConvertida = append(matrizConvertida, i)
	    }
	}
	return matrizConvertida, ordem
}

func multiplicacaoMatriz(matriz1 []float64, matriz2 []float64, ordemMatriz int) ([]float64){
	var matriz3 []float64
	var val float64
	for i := 0; i < ordemMatriz; i++ {
		for j := 0; j < ordemMatriz; j++ {
			val = 0
			for k := 0; k < ordemMatriz; k++ {
				val += matriz1[i*ordemMatriz + k] * matriz2[k*ordemMatriz + j]
			}
			matriz3 = append(matriz3, val)			
		}
	}
	return matriz3
}

func salvarMatriz(matriz []float64, ordem int, NomeMatriz string) (error){
	var linhas []string
	arquivo, err := os.Create(NomeMatriz)
	if err != nil {
		log.Fatalf("Erro:", err)
	}
	linhas = append(linhas, strconv.Itoa(ordem)+"\n")

	for i := 0; i < ordem; i++ {
		linhas = append(linhas, "\n")
		for j := 0; j < ordem; j++{
				linhas = append(linhas, strconv.FormatFloat(matriz[ordem*i + j],'f',1,64))
				if j != ordem - 1 {
					linhas = append(linhas, ":")
				} 
		}		
	}

	defer arquivo.Close()
	escritor := bufio.NewWriter(arquivo)
	for _, linha := range linhas {
		fmt.Fprint(escritor, linha) 
	}
	fmt.Fprintln(escritor,"")
	return escritor.Flush()
}
//