// Este programa define dois vetores e adiciona ambos. 
// Primeiro, ele faz a soma serial.
// Depois particiona os vetores de entrada e faz a soma paralela. 
package main
import (
   "fmt"
   "os"
   "strconv"
)

func main() {
   tamanhoDosVetores, err := strconv.Atoi(os.Args[1])
   if err != nil {
      fmt.Println("Erro de conversão.")
   }

   vetor1 := make([]int, tamanhoDosVetores)
   vetor2 := make([]int, tamanhoDosVetores)
   for i := 1; i < tamanhoDosVetores; i++ {
      vetor1[i] = vetor1[i-1] + 1
      vetor2[i] = vetor2[i-1] + 1
   }
   fmt.Printf("Vetor 1: %v\n", vetor1)
   fmt.Printf("Vetor 2: %v\n", vetor2)
   fmt.Printf("Soma serial:\n")
   fmt.Printf("Vetor 1 + Vetor 2: %v\n", vectorAdd(vetor1, vetor2))

   sync1 := make(chan []int)
   sync2 := make(chan []int)
   go vectorAddParallel(vetor1[:len(vetor1)/2], vetor2[:len(vetor2)/2], sync1)
   go vectorAddParallel(vetor1[len(vetor1)/2:], vetor2[len(vetor2)/2:], sync2)
   c1 := <-sync1
   c2 := <-sync2
   fmt.Printf("Soma paralela:\n")
   fmt.Printf("Vetor 1 + Vetor 2: %v\n", append(c1, c2...))
}

func vectorAdd(a []int, b []int) ([]int) {
   c := make([]int, len(a))
   for i := 0; i < len(a); i++ {
      c[i] = a[i] + b[i]
   }
   return c
}

func vectorAddParallel(a []int, b []int, sync chan []int) {
   c := make([]int, len(a))
   for i := 0; i < len(a); i++ {
      c[i] = a[i] + b[i]
   }
   sync <- c
}
