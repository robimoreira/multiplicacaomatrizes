package main

import (
   "fmt"
   "runtime"

)

func multiplica(id int, sync chan []float64) {
   fmt.Printf("Iniciando rotina %d.\n", id)
   matriz3 := make([]float64, 10)
   for i := 0; i < 10; i++ {
      matriz3[i] = float64(id)
   }
   fmt.Printf("Finalizando rotina %d.\n", id)
   sync <- matriz3
}

func main() {
   sync := make([]chan []float64, runtime.GOMAXPROCS(runtime.NumCPU()))
   for i := 0; i < runtime.GOMAXPROCS(runtime.NumCPU()); i++ {
      sync[i] = make(chan []float64)
   }

   for i := 0; i < runtime.GOMAXPROCS(runtime.NumCPU()); i++ {
      go multiplica(i, sync[i])
   }

   var c []float64
   for i := 0; i < runtime.GOMAXPROCS(runtime.NumCPU()); i++ {
      matriz3 = append(matriz3, <-sync[i]...)
   }
   fmt.Println(matriz3)
}
//